package project.autogantt;

import javafx.scene.*;
import javafx.scene.control.Label;
import javafx.stage.*;

public class InfoPopup {

	public static void display(){
		final Stage popup = new Stage();
		popup.initModality(Modality.NONE);
		popup.initStyle(StageStyle.UTILITY);
		popup.setTitle("AutoGantt - Info");
		Scene scene = new Scene(new Label("     Created by Ewan Porter 2020"), 200, 80);
		popup.setScene(scene);
		popup.focusedProperty().addListener((obs, wasFocused, isNowFocused) -> {
			if (! isNowFocused) {
				popup.hide();
			}
		});
		popup.showAndWait();
	}

}

