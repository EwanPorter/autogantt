package project.autogantt;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.category.IntervalCategoryDataset;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;

import javafx.collections.ObservableList;
import project.autogantt.model.TaskData;

public class CreateGantt{

	private static TaskSeriesCollection collectionGenerator(ObservableList<TaskData> taskList){
		TaskSeriesCollection tasksCollection = new TaskSeriesCollection();
		TaskSeries tasks = new TaskSeries("");
		for (TaskData task : taskList) {
			tasks.add(new Task(task.getTaskName(), 
					task.getStartDate(), 
					task.getEndDate()));
		}		
		tasksCollection.add(tasks);
		return tasksCollection;
	}

	private static JFreeChart generateChart(IntervalCategoryDataset dataset){
		JFreeChart chart = ChartFactory.createGanttChart(
				"",
				"Task",
				"Date",
				dataset,
				false,
				false, 
				false
				);
		return chart;
	}

	public static JFreeChart createChart(ObservableList<TaskData> taskList){
		IntervalCategoryDataset tasksCollection = collectionGenerator(taskList);
		JFreeChart ganttChart = generateChart(tasksCollection);
		ganttChart.setBorderPaint(null);
		ganttChart.setBackgroundPaint(null);
		return ganttChart;
	}


}
