package project.autogantt;

import java.io.File;
import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;

import project.autogantt.model.TaskData;
import project.autogantt.model.TaskList;

public class LoadGantt {
	static MainApp mainApp = new MainApp();

	@SuppressWarnings("static-access")
	public static ObservableList<TaskData> taskSerialiser(ObservableList<TaskData> tasks){
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("XML Files", "*.xml"));
		File ganttLoad = fileChooser.showOpenDialog(mainApp.getPrimaryStage());
		Serializer serializer = new Persister();
		List<TaskData> tasksList = null;
		try {
			TaskList list = serializer.read(TaskList.class, ganttLoad);
			tasksList = list.getTasks();
		} catch (Exception e) {
			e.printStackTrace();
		}
		ObservableList<TaskData> list = FXCollections.observableArrayList(tasksList);
		tasks = list;
		return tasks;
	}
}