package project.autogantt;

import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.io.IOException;

import javax.swing.ImageIcon;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class MainApp extends Application {

	private Stage primaryStage;
    private static BorderPane rootLayout;
    private static java.awt.Image image = new ImageIcon(MainApp.class.getResource("AutoGantt-icon.png")).getImage();
    private static TrayIcon trayIcon = new TrayIcon(image);
    private static SystemTray tray = SystemTray.getSystemTray();

    @Override
    public void start(Stage primaryStage) {
    	Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("AutoGantt");
        this.primaryStage.setResizable(false);
        this.primaryStage.setX(primaryScreenBounds.getMinX() + primaryScreenBounds.getWidth() - 600);
        this.primaryStage.setY(primaryScreenBounds.getMinY() + primaryScreenBounds.getHeight() - 325);
        Platform.setImplicitExit(false);
        primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("AutoGantt-icon.png")));

        javax.swing.SwingUtilities.invokeLater(this::addAppToTray);

        initRootLayout();
        showHomeView();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();
            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Shows the home view inside the root layout.
     */
    public static void showHomeView() {
        try {
            // Load home view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/HomeView.fxml"));
            AnchorPane homeView = (AnchorPane) loader.load();
            // Set home view into the center of root layout.
            rootLayout.setCenter(homeView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the home stage.
     * @return
     */

    /**
     * Shows the create/modify gantt view inside the root layout.
     */
    public static void showCreateModGanttView() {
        try {
            // Load Create / Mod Gantt view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/CreateModGanttView.fxml"));
            AnchorPane createModGanttView = (AnchorPane) loader.load();
            // Set home view into the center of root layout.
            rootLayout.setCenter(createModGanttView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the create/mod gantt stage.
     * @return
     */

    /**
     * Shows the view Gantt view inside the root layout.
     */
    public static void showGanttView() {
        try {
            // Load Gantt view.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(MainApp.class.getResource("view/ViewGanttView.fxml"));
            AnchorPane ganttView = (AnchorPane) loader.load();
            // Set home view into the center of root layout.
            rootLayout.setCenter(ganttView);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the view gantt stage.
     * @return
     */

    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Sets up a system tray icon for the application.
     */
    private void addAppToTray() {
        try {
            // ensure awt toolkit is initialized.
            java.awt.Toolkit.getDefaultToolkit();

            // app requires system tray support, just exit if there is no support.
            if (!SystemTray.isSupported()) {
                System.out.println("No system tray support, application exiting.");
                Platform.exit();
            }

            // if the user double-clicks on the tray icon, show the main app stage.
            trayIcon.addActionListener(event -> Platform.runLater(this::showStage));

            // if the user selects the default menu item (which includes the app name),
            // show the main app stage.
            java.awt.MenuItem openItem = new java.awt.MenuItem("Open AutoGantt");
            openItem.addActionListener(event -> Platform.runLater(this::showStage));

            // to really exit the application, the user must go to the system tray icon
            // and select the exit option, this will shutdown JavaFX and remove the
            // tray icon (removing the tray icon will also shut down AWT).
            java.awt.MenuItem exitItem = new java.awt.MenuItem("Exit");
            exitItem.addActionListener(event -> {
                exit();
            });

            // setup the popup menu for the application.
            final java.awt.PopupMenu popup = new java.awt.PopupMenu();
            popup.add(openItem);
            popup.addSeparator();
            popup.add(exitItem);
            trayIcon.setPopupMenu(popup);

            // add the application tray icon to the system tray.
            tray.add(trayIcon);
        } catch (java.awt.AWTException e) {
            System.out.println("Unable to init system tray");
            e.printStackTrace();
        }
    }

    private void showStage() {
        if (primaryStage != null) {
            primaryStage.show();
            primaryStage.toFront();
        }
    }

    public static void exit() {
    	SystemTray newTray = SystemTray.getSystemTray();
    	Platform.exit();
    	newTray.remove(trayIcon);
    }

}
