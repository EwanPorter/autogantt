package project.autogantt.view;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import project.autogantt.MainApp;
import project.autogantt.model.TaskData;

public class CreateModGanttController {
	private String taskName;

	private Date startDate;
	private Date endDate;

	private File programFile;

	@FXML
	private Button selectProgramButton;

	@FXML
	private Button addTaskButton;
	
	@FXML
	private Button deleteTaskButton;

	@FXML
	private Button createGanttButton;

	@FXML
	private TextField taskNameField;

	@FXML
	private DatePicker endDatePicker;

	@FXML
	private DatePicker startDatePicker;

	@FXML
	private Label nameLabel;
	
	@FXML
	private Label startLabel;
	
	@FXML
	private Label endLabel;
	
	@FXML
	private Label programLabel;
	
	@FXML
	private Label createLabel;

	@FXML
	private TableView<TaskData> taskTable;

	@FXML
	public void handleDatePickerAction(ActionEvent event) throws Exception{
		if(event.getSource() == startDatePicker){
			LocalDate startDateLocal = startDatePicker.getValue();
			startDate = convertToDateViaInstant(startDateLocal);
		}
		else if(event.getSource() == endDatePicker){
			LocalDate endDateLocal = endDatePicker.getValue();
			Date end = convertToDateViaInstant(endDateLocal);
			if(end.after(startDate)) {
				endDate = convertToDateViaInstant(endDateLocal);
			}
			else {
				endLabel.setText("End date must be after Start date");
			}
			
		}
	}

	@FXML
	public void handleButtonAction(ActionEvent event) throws Exception {
		boolean bool = false;
		if(event.getSource() == createGanttButton) {
			if(!TaskData.getTaskList().isEmpty()) {
				ArrayList<TaskData> array = new ArrayList<TaskData>(TaskData.getTaskList());
				TaskData.setTasksList(array);
				MainApp.showGanttView();
			}
			else {
				createLabel.setText("No tasks input.");
			}
		}
		else if(event.getSource() == addTaskButton){
			if(taskNameField.getText().isEmpty()) {
				nameLabel.setText("Task name must be entered.");		
			}
			else if(startDate == null) {
				startLabel.setText("Start date must be selected.");
			}
			else if(endDate == null) {
				endLabel.setText("End date must be selected.");
			}
			else if(programFile == null) {
				programLabel.setText("Program must be selected.");
			}
			else {
				taskName = taskNameField.getText();
				for(TaskData task : TaskData.getTaskList()) {
					if(task.getTaskName().equals(taskName)) {
						nameLabel.setText("Select unique task name.");
						bool = true;
					}
				}
				if(!bool) {
					nameLabel.setText("");
					startLabel.setText("");
					endLabel.setText("");	
					TaskData.addTaskList(taskName, startDate, endDate, programFile);
					tablePopulate();
				}
			}
		}
		else if(event.getSource() == selectProgramButton){
			programFile = programFileSelecter();
			String programString;
			programString = programFile.getName();
			programLabel.setText(programString);
		}
		else if(event.getSource() == deleteTaskButton) {
			if(taskTable.getSelectionModel().getSelectedItem() != null) {
				TaskData selectedTask = taskTable.getSelectionModel().getSelectedItem();
				taskTable.getItems().remove(selectedTask);
				boolean bool2 = false;
				while(!bool2) {
					for(TaskData task : TaskData.getTaskList()) {
						if(task.getTaskID() == selectedTask.getTaskID()) {
							TaskData.getTaskList().remove(selectedTask.getTaskID());
							bool2 = true;
						}
					}
				}
			}
			else {
				createLabel.setText("No tasks selected to delete.");
			}
		}
		else{
			System.out.println("Source unknown");
		}
	}

	@FXML
	public void handleTextFieldAction(ActionEvent event) throws Exception {
		if(event.getSource() == taskNameField){
			taskNameField.setText("");
		}
		else {
			System.out.println("Source unknown");
		}
	}

	@FXML
	public void handleOnClickAction(MouseEvent event){
		if(event.getSource() == taskNameField){
			if(taskNameField.getText().equals("Enter Task Name")) {
				taskNameField.setText("");
			}
		}
		else {
			System.out.println("Source unknown");
		}
	}

	public void tablePopulate(){
		taskTable.getItems().clear();
		for(TaskData task : TaskData.getTaskList()){
			taskTable.getItems().add(task);
		}
	}
	
	@FXML
	public void initialize() {
		try{
			TableColumn<TaskData, String> nameColumn = new TableColumn<>("Task Name");
			nameColumn.setMinWidth(taskTable.getPrefWidth()/2);
			nameColumn.setCellValueFactory(new PropertyValueFactory<TaskData, String>("taskName"));
			
			TableColumn<TaskData, File> programColumn = new TableColumn<>("Program");
			programColumn.setMinWidth(taskTable.getPrefWidth()/2);
			programColumn.setCellValueFactory(new PropertyValueFactory<TaskData, File>("programLocation"));
			
			taskTable.getColumns().add(nameColumn);
			taskTable.getColumns().add(programColumn);
			tablePopulate();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Date convertToDateViaInstant(LocalDate dateToConvert) {
	    return java.util.Date.from(dateToConvert.atStartOfDay()
	      .atZone(ZoneId.systemDefault())
	      .toInstant());
	}

	public File programFileSelecter(){
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("Executable Files", "*.exe"));
		File programFile = fileChooser.showOpenDialog(null);
		return programFile;
	}
}
