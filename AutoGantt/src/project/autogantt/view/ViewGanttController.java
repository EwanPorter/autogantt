package project.autogantt.view;

import project.autogantt.CreateGantt;
import project.autogantt.MainApp;
import project.autogantt.SaveGantt;
import project.autogantt.model.TaskData;

import org.jfree.chart.JFreeChart;
import org.jfree.chart.fx.ChartViewer;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class ViewGanttController {

	@FXML
	private Button modGanttButton;
	
	@FXML
	private Button saveGanttButton;

	@FXML
	private ChartViewer ganttChartPanel;
	
	public void initialize(){
		JFreeChart gantt = CreateGantt.createChart(TaskData.getTaskList());
		ganttChartPanel.setChart(gantt);
		ganttChartPanel.setVisible(true);
	}

	@FXML
	public void handleButtonAction(ActionEvent event) throws Exception {
		if(event.getSource() == modGanttButton) {
			MainApp.showCreateModGanttView();
		}
		else if(event.getSource() == saveGanttButton) {
			SaveGantt.taskSerialiser(TaskData.getTasksList());
		}
		else{
			System.out.println("Source unknown");
		}
	}
}
