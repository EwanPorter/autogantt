package project.autogantt.view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import project.autogantt.InfoPopup;
import project.autogantt.MainApp;

public class RootLayoutController {

	@FXML
	private MenuItem homeButton, infoButton, exitButton;

	@FXML
	public void handleButtonAction(ActionEvent event) throws Exception {
		if(event.getSource() == homeButton) {
			MainApp.showHomeView();
		}
		else if(event.getSource() == infoButton){
			InfoPopup.display();
		}
		else if(event.getSource() == exitButton) {
			MainApp.exit();
		}
		else{
			System.out.println("Source unknown");
		}
}
}
