package project.autogantt.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import project.autogantt.LoadGantt;
import project.autogantt.MainApp;
import project.autogantt.model.TaskData;

public class HomeViewController {

	@FXML
	private Button createGanttButton;
	
	@FXML
	private Button viewGanttButton;
	
	@FXML 
	private Label loadLabel;

	@FXML
	public void handleButtonAction(ActionEvent event) throws Exception {
			if(event.getSource() == createGanttButton) {
				TaskData.clearTaskList();
				MainApp.showCreateModGanttView();
			}
			else if(event.getSource() == viewGanttButton){
				TaskData.clearTaskList();
				try {
					ObservableList<TaskData> tasks = FXCollections.observableArrayList();
					tasks = LoadGantt.taskSerialiser(TaskData.getTaskList());
					TaskData.setTaskList(tasks);
				}
				catch (Exception e) {
					loadLabel.setText("Could not load XML File.");
				}
				finally {
					if(!TaskData.getTaskList().isEmpty()) {
						MainApp.showGanttView();
					}
					else {
						loadLabel.setText("Could not load XML File.");
					}
				}
			}
			else{
				System.out.println("Source unknown");
			}
	}
}
