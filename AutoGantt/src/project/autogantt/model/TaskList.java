package project.autogantt.model;

import java.util.ArrayList;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root
public class TaskList{
	
	@ElementList
	private static ArrayList<TaskData> tasks;
	
	public TaskList() {
		super();
	}
	
	public TaskList(ArrayList<TaskData> tasks) {
		TaskList.tasks = tasks;
	}

	public static ArrayList<TaskData> getTasks() {
		return tasks;
	}

	public static void setTasks(ArrayList<TaskData> tasks) {
		TaskList.tasks = tasks;
	}
}
