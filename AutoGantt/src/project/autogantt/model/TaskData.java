package project.autogantt.model;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

@Root
public class TaskData{

	@Attribute
	private int taskID;

	@Element
	private String taskName;
	@Element
	private Date startDate;
	@Element
	private Date endDate;
	@Element
	private File programLocation;
	
	public TaskData() {
		super();
	}

	public TaskData(int taskID, String taskName, Date startDate, Date endDate, File programLocation){
		this.taskID = taskID;
		this.taskName = taskName;
		this.startDate = startDate;
		this.endDate = endDate;
		this.programLocation = programLocation;
	}

	private static ObservableList<TaskData> taskList = FXCollections.observableArrayList();
	
    public static void setTaskList(ObservableList<TaskData> taskList) {
		TaskData.taskList = taskList;
		ArrayList<TaskData> array = new ArrayList<TaskData>(taskList);
		TaskList.setTasks(array);
	}
    
    public static void addTaskList(String taskName, Date startDate, Date endDate, File programLocation){
    	TaskData task = new TaskData(TaskData.getTaskList().size(), taskName, startDate, endDate, programLocation);
    	TaskData.getTaskList().add(task);
    };

	public static ObservableList<TaskData> getTaskList() {
		return taskList;
	}
	
	public static void clearTaskList() {
		ObservableList<TaskData> list = FXCollections.observableArrayList();
		setTaskList(list);
	}
	
	public static ArrayList<TaskData> getTasksList() {
		ArrayList<TaskData >tasks = TaskList.getTasks();
		return tasks;
	}
	
	public static void setTasksList(ArrayList<TaskData> tasks) {
		TaskList.setTasks(tasks);
	}

	public int getTaskID() {
		return taskID;
	}
	
	public String getTaskName() {
		return taskName;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	
	public Date getEndDate() {
		return endDate;
	}
	
	public File getProgramLocation() {
		return programLocation;
	}

}
