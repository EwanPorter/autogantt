package project.autogantt;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import project.autogantt.model.TaskData;
import project.autogantt.model.TaskList;
import project.autogantt.MainApp;

public class SaveGantt {
	static MainApp mainApp = new MainApp();

	public static void taskSerialiser(List<TaskData> tasks) throws IOException{
		Serializer serializer = new Persister();
		FileChooser fileChooser = new FileChooser();
		fileChooser.getExtensionFilters().addAll(new ExtensionFilter("XML Files", "*.xml"));
		File ganttSave = fileChooser.showSaveDialog(mainApp.getPrimaryStage());
		StringWriter writer = new StringWriter();
		TaskList list = new TaskList(TaskData.getTasksList());
		try {
			serializer.write(list, writer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		FileWriter fileWriter = new FileWriter(ganttSave);
		fileWriter.write(writer.getBuffer().toString());
		fileWriter.close();
	}

}
